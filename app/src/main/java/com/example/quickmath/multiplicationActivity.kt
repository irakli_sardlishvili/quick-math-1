package com.example.quickmath

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_multiplication.*
import kotlinx.android.synthetic.main.activity_plus_minus.*
import kotlinx.android.synthetic.main.activity_plus_minus.button0
import kotlinx.android.synthetic.main.activity_plus_minus.button1
import kotlinx.android.synthetic.main.activity_plus_minus.button2
import kotlinx.android.synthetic.main.activity_plus_minus.button3
import kotlinx.android.synthetic.main.activity_plus_minus.button4
import kotlinx.android.synthetic.main.activity_plus_minus.button5
import kotlinx.android.synthetic.main.activity_plus_minus.button6
import kotlinx.android.synthetic.main.activity_plus_minus.button7
import kotlinx.android.synthetic.main.activity_plus_minus.button8
import kotlinx.android.synthetic.main.activity_plus_minus.button9
import kotlinx.android.synthetic.main.activity_plus_minus.goAndAnswerButton
import kotlinx.android.synthetic.main.activity_plus_minus.resultTextView
import kotlinx.android.synthetic.main.activity_plus_minus.taskTextView

class multiplicationActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multiplication)
        init()
    }
    private fun init() {
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
        taskTextView.text = ""
        resultTextView.text = ""
        taskNumber = 0
        task = ""
        answer = 0
        score = 0
    }

    override fun onClick(v: View?) {
        if (resultTextView.text.length <= 2 && taskNumber != 0) {
            val button = v as Button
            resultTextView.text = resultTextView.text.toString() + button.text.toString()
        }
    }

    fun negativeNumber(view: View) {
        if (resultTextView.text.isEmpty()) {
            resultTextView.text = "-"
        }
    }

    fun delete(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            resultTextView.text = value.substring(0, value.length - 1)
        }
    }
    private var taskNumber = 0
    private var task = ""
    private var answer = 0
    private var score = 0

    fun goAndAnswerButtonListener(view: View) {
        if (taskNumber != 10) {
            if (taskNumber == 0) {
                goAndAnswerButton.text = "answer"
                generateTask()
                taskTextView.text = task
                taskNumber += 1
            }else {
                checkAnswer()
                generateTask()
                taskTextView.text = task
                resultTextView.text = ""
                taskNumber += 1
            }
        }else{
            checkAnswer()
            goToScoreActivity()
        }
    }
    private val numbs = arrayOf(2, 3, 4, 5, 6, 7, 8, 9, 10)

    private fun generateNumberToMultiply(): Int {
        return numbs.random()
    }

    private fun generateTask() {
        val firstNumb = generateNumberToMultiply()
        val secondNumb = generateNumberToMultiply()

        task = firstNumb.toString() + " x " + secondNumb.toString() + " ="
        answer = firstNumb * secondNumb
    }

    private fun checkAnswer() {
        if (resultTextView.text == answer.toString()) {
            score += 1
        }
    }

    private fun goToScoreActivity() {
        val finalScore = score.toString()

        val intent = Intent(this, scoreActivity::class.java)
        intent.putExtra("finalScore", finalScore)
        startActivity(intent)
    }
}
