package com.example.quickmath

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat.startActivity
import kotlinx.android.synthetic.main.activity_score.*

class scoreActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score)
        init()
    }
    private fun init() {
        val score = intent.extras?.getString("finalScore", "")
        scoreTextView.text = "Score: " + score + "/10"

        tryAgainButton.setOnClickListener() {
            tryAgain()
    }
        selectModeButton.setOnClickListener() {
            selectMode()
        }
    }

    fun tryAgain() {
        val intent = Intent(this, PlusMinusActivity::class.java)
        startActivity(intent)
    }
    fun selectMode() {
        val intent = Intent(this, SelectModeActivity::class.java)
        startActivity(intent)
    }
}