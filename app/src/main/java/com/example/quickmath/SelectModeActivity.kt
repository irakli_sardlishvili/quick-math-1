package com.example.quickmath

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_select_mode.*

class SelectModeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_mode)
        init()
    }
    private fun init() {
        plusMinus.setOnClickListener() {
            plusMinusPage()
        }
        multiplication.setOnClickListener() {
            multiplyDividePage()
        }
    }
    private fun plusMinusPage() {
        val intent = Intent(this, PlusMinusActivity::class.java)
        startActivity(intent)
    }
    private fun multiplyDividePage() {
        val intent = Intent(this, multiplicationActivity::class.java)
        startActivity(intent)
    }
}