package com.example.quickmath

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_plus_minus.*

class PlusMinusActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plus_minus)
        init()
    }
    private fun init() {
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
        taskTextView.text = ""
        resultTextView.text = ""
        taskNumber = 0
        task = ""
        answer = 0
        score = 0
    }

    override fun onClick(v: View?) {
        if (resultTextView.text.length <= 2 && taskNumber != 0) {
            val button = v as Button
            resultTextView.text = resultTextView.text.toString() + button.text.toString()
        }
    }

    fun negativeNumber(view: View) {
        if (resultTextView.text.isEmpty()) {
            resultTextView.text = "-"
        }
    }

    fun delete(view: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            resultTextView.text = value.substring(0, value.length - 1)
        }
    }
    private var taskNumber = 0
    private var task = ""
    private var answer = 0
    private var score = 0

    fun goAndAnswerButtonListener(view: View) {
        if (taskNumber != 10) {
            if (taskNumber == 0) {
                goAndAnswerButton.text = "answer"
                generateTask()
                taskTextView.text = task
                taskNumber += 1
            }else {
                checkAnswer()
                generateTask()
                taskTextView.text = task
                resultTextView.text = ""
                taskNumber += 1
            }
        }else{
            checkAnswer()
            goToScoreActivity()
        }
    }
    private val numbs = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20)
    private val operations = arrayOf("+", "-")

    private fun generateRandomNumber(): Int {
        return numbs.random()
    }
    private fun generateRandomOperation(): String {
        return operations.random()
    }

    private fun generateTask() {
        val firstNumb = generateRandomNumber()
        val secondNumb = generateRandomNumber()
        val thirdNumb = generateRandomNumber()
        val firstOperation = generateRandomOperation()
        val secondOperation = generateRandomOperation()
        task = firstNumb.toString() + " " + firstOperation + " " + secondNumb.toString() +
                " " + secondOperation + " " + thirdNumb.toString() + " ="

        if (firstOperation == "+") {
            if (secondOperation == "+") {
                answer = firstNumb + secondNumb + thirdNumb
            }else {
                answer = firstNumb + secondNumb - thirdNumb
            }
        }else if (secondOperation == "+") {
            answer = firstNumb - secondNumb + thirdNumb
        }else {
            answer = firstNumb - secondNumb - thirdNumb
        }
    }

    private fun checkAnswer() {
        if (resultTextView.text == answer.toString()) {
            score += 1
        }
    }

    private fun goToScoreActivity() {
        val finalScore = score.toString()

        val intent = Intent(this, scoreActivity::class.java)
        intent.putExtra("finalScore", finalScore)
        startActivity(intent)
    }
}
