package com.example.quickmath

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init() {
        startButton.setOnClickListener() {
            nextPage()
        }
    }
    private fun nextPage() {
        val intent = Intent(this, SelectModeActivity::class.java)
        startActivity(intent)
    }
}